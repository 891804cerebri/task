// Этапы разработки: 
// 1. Создание массива объектов tasks
// 2. Получить в константы через queryselector ссылки на тэги
// 3. Создание функции renderTask - функции по генерации обшей разметки;
// 3.1. Создание функции по генерации карточки задания:
// 3.1.1. создание функции по созданию тэга разметки - createTag;
// 3.1.2. создание функции по созданию разметки - getTaskCard;
// 3.1.3. создание функции по отрисовке и отображению информации из массива объектов - renderTask;
// 4. В функции getTaskCard - создаем тэг input, присваем ему checkbox и задаем ему checked;
// 4.1. помещаем созданный input с помощью appendChild в taskHtml(div);
//5. Создаем в области глобальных констант через querySelector получаем поля title, description и add (button)
//6. Вешаем на полученные глобальные константы слушатели (click и keypress).


// Array object
let tasks = [
    {
        id: 1,
        title: 'Поменять колодки',
        description: 'Через 10 000',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 2,
        title: 'Поменять масло',
        description: 'Через 8 000',
        date: new Date().getTime(),
        isComplete: false
    },
    {
        id: 3,
        title: 'Поменять топливный фильтр',
        description: 'Через 16 000',
        date: new Date().getTime(),
        isComplete: false
    },
];

// Глобальные константы в которых храняться ссылки на тэги

const tasksTag = document.querySelector('.tasks');
const titleInput = document.querySelector('#title');
const descriptionInput = document.querySelector('#description');
const addButton = document.querySelector('#add');


//Listener 

addButton.addEventListener('click', addTask);
titleInput.addEventListener('keypress', titleEnterHandle);
descriptionInput.addEventListener('keypress', descriptionEnterHandle);


// Use Functions

renderTask(tasks);
getNewId();

//Functions

//Функция по генерации тэгов. Используется в функции getTaskCard которая генерирует разметку
function createTag(tagName, className, value) {
    const tag = document.createElement(tagName);
    tag.classList.add(className); //classlist - геттер. Возвращает объект. Имеет методы: add, remove, toogle, item.  
    if (value) {
        tag.innerText = value;
    }
    return tag
}

//Функция по генерации разметки карточки задания. Используется в функции renderTask которая формирует 1 карточку задания
function getTaskCard(taskObj) {
    //тэги task
    const taskHtml = createTag('div', 'task');
    const titleHtml = createTag('span', 'title', taskObj.title);
    const subsHtml = createTag('span', 'subs', taskObj.description);
    const deleteBtn = createTag('div', 'delete', 'X');
    //создание текущего времени
    const friendlyDate = moment(taskObj.date).format('DD/MM/YYYY HH:mm:ss');
    const dateTimeHtml = createTag('span', 'dateTime', friendlyDate);

    //тэги addTask
    const inputHtml = createTag('input');
    inputHtml.type = 'checkbox';
    inputHtml.checked = taskObj.isComplete;



    //присваивание id

    deleteBtn.id = taskObj.id;

    //вешаем слушатель

    deleteBtn.addEventListener('click', deleteItem);

    //appendChild - добавляет элемент в качестве вложенного в указанный узел
    taskHtml.appendChild(deleteBtn);
    taskHtml.appendChild(titleHtml);
    taskHtml.appendChild(subsHtml);
    taskHtml.appendChild(dateTimeHtml);

    taskHtml.appendChild(inputHtml);


    return taskHtml;
}

//функция по заполнению разметки карточки задания данными из массива объектов

function renderTask(tasksArr) {
    tasksTag.innerHTML = '';
    tasksArr.forEach(item => {
        const taskCardHtml = getTaskCard(item);
        tasksTag.appendChild(taskCardHtml);
    });
};

// функция по удалению элемента. Используется в функции getTaskCard для обработки нажатия на кнопку удаления задания 

function deleteItem(event) {
    const id = +event.target.id;
    tasks = tasks.filter(item => item.id != id);
    renderTask(tasks);

}


function titleEnterHandle(event) {
    if (event.keyCode === 13) {
        descriptionInput.focus(); //фокусировка. Приготовится к вводу данных на элементе. Метод elem.blur() - снимает фокус с элемента
    }
};

function descriptionEnterHandle(event) {
    if (event.keyCode === 13) {
        checkTitle(); 
        titleInput.focus(); 
    }
}; 

/////

//функция по добавлению задачи

function addTask() {
    const titleInput = document.querySelector('#title');
    const descriptionInput = document.querySelector('#description');

    const title = titleInput.value; // получает данные из значения titleInput
    const description = descriptionInput.value;/// получает данные из значения descriptionInput

    const newTask = {
        id: 100,
        title: title,
        deskription: description,
        date: new Date().getTime(),
        isComplete: false
    }
    console.log(newTask); 
};

//функция по добавлению нового ID

function getNewId() {
    if (tasks.length > 0) {
        const ids = tasks.map(item => item.id);
        const max = Math.max(...ids);
        return max + 1;
    }
};

// Функция по проверке наличия значений в инпуте Title. Если поле пустое - добавляет класс required 

function checkTitle () {
    if (titleInput.value.lenght > 0) {
        addTask(); 
        titleInput.classList.remove('required'); // удаляет подкрашивание красным. класс required 
    } else {
        titleInput.classList.add('required'); 
    }
}

// Функция по чистке input

function clearFields (titleField, descrField) {
    titleField.value = ''; 
    descrField.value = ''; 
} 

// функция которая записывает в локальное хранилище браузера данные 

function getNewTask () {
    const data = localStorage.getItem('tasks'); // получить данные из пары ключ-значение
    if (data) {
        const parsedData = JSON.parse(data); 
        return parsedData;
    }
    return []; 
}

// Функция которая 

function changeTask() {
    const taskString = JSON.stringify(tasks); // преобразование JSON в строку
    localStorage.setItem('tasks', taskString);  // сохранить пару ключ-значение
}
